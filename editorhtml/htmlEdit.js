var old = '';
var isSaved = true;
var firstTime = true;

function update() {
	if (!isSaved) {
		window.onbeforeunload = function() {
			return 'You have unsaved changes. Are you sure you want to leave?';
		}
	}
	var docObj = window.frames['updateFrame'].document;
	var textarea = document.getElementById('HTMLEditBox');
	
	if(old != textarea.value) {
		if (firstTime) {
			firstTime = false;
		} else {
			isSaved = false;
		}
		old = textarea.value
		docObj.open('text/html', 'replace');
		docObj.write(old);
		docObj.close();
	}
	window.setTimeout(update, 150)
}
