<?php 
class jd{
public function jd(){

}
public function openhtml()
{
echo "<html>";
}
public function closehtml()
{
echo "</html>";
}

public function openhead()
{
echo "<head>
<meta charset='UTF-8'>
<title></title>
<link rel='stylesheet' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css'>
<link rel='stylesheet' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css'>
<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>
<script src='http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js'></script>
<style type='text/css'>
    .bs-example{
    	margin: 20px;
    }
</style>";
}
public function closehead()
{
echo "</head>"; 
}

public function openbody($body)
{
echo "<body>".$body;
}

public function closebody()
{
echo "</body>";
}

public function br()
{
echo "<br>";    
}

public function opentable($class)
{
echo "<table class='".$class."'>";
}

public function closetable()
{
echo "</table>";
}

public function openrow($clase)
{
echo "<tr class='".$clase."'>";
}

public function closerow()
{
echo "</tr>";
}

public function opencol($dato,$clase)
{
echo "<td class='".$clase."'>".$dato;
}

public function closecol()
{
echo "</td>";
}

public function openlabel($dato)
{
echo "<span class='label label-default'>".$dato;
}

public function closelabel()
{
echo "</span>";  
}

public function openicon($class)
{
echo "<span class='".$class."'>";
}
public function closeicon()
{
echo "</span>";
}

public function openbutton($value,$class)
{
  echo "<button type='button' class='btn btn-default'>".$value;
}
public function closebutton()
{
    echo "</button>";
}

public function opendropdownbutton($list)
{
  
  $cantidad=count($list); 
  
  echo "<div class='btn-group'>
  <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown'>
  Action <span class='caret'></span>
  </button>
  <ul class='dropdown-menu' role='menu'>";
  
  for($z=0;$z<$cantidad;$z++)
        {
        if ($list[$z]=="/")
        {
        echo "<li class='divider'></li>";
        }
        else 
        {    
        echo "<li><a href='#'>".$list[$z]."</a></li>";
        }
        
        }
    
   
  echo "</ul></div>";
}

public function closedropdownbutton()
{
    echo "</div>";
}

public function openinput($value)
{
  echo "<div class='input-group'>
  <span class='input-group-addon'>".$value."</span>
  <input type='text' class='form-control'>
  <span class='input-group-addon'></span>
  ";
}

public function closeinput()
{
  echo "</div>";  
}

public function opentabs()
{   
    //tab nombre
    $miarray=array("red","orange","yellow","green","blue");
    //tab titulo
    $tabtitulo=array("Red","Orange","Yellow","Green","blue");
    //tab content, parrafo
    $tabcontent=array("red red red red red red","orange orange orange orange orange","yellow yellow yellow yellow yellow","green green green green green","blue blue blue blue blue");
    
    
    
    $cantidad=count($miarray);
    $active=0;
    
    
    
    echo "<div id='content'>
    <ul id='tabs' class='nav nav-tabs' data-tabs='tabs'>";
        
        for($i=0;$i<$cantidad;$i++)
        {
         if ($active==$i)
         {
            echo "<li class='active'><a href='#".$miarray[$i]."' data-toggle='tab'>".$miarray[$i]."</a></li>"; 
         }
         else
         {
            echo "<li><a href='#".$miarray[$i]."' data-toggle='tab'>".$miarray[$i]."</a></li>";
         }
        }
    
    echo "</ul>
    
<div id='my-tab-content' class='tab-content'>";

        for($i=0;$i<$cantidad;$i++)
        {
            if ($active==$i)
            {
            echo "<div class='tab-pane active' id='".$miarray[$i]."'>
            <h1>".$tabtitulo[$i]."</h1>
            <p>".$tabcontent[$i]."</p>
        </div>";
            }
            else
            {
            echo "<div class='tab-pane' id=".$miarray[$i].">
            <h1>".$tabtitulo[$i]."</h1>
            <p>".$tabcontent[$i]."</p>
        </div>";
            }
        }

        
    echo "</div>";
    
}

public function closetabs()
        {
        echo "</div>";
        }

public function Badge($texto,$badge)
        {        
        echo "<a href='#'>$texto <span class='badge'>$badge</span></a>";
        }
        
        public function alert($tipo,$texto)
        {
            if($tipo=="success")
            echo "<div class='alert alert-success'>$texto</div>";
            if($tipo=="info")
            echo "<div class='alert alert-info'>$texto</div>";
            if($tipo=="warning")
            echo "<div class='alert alert-warning'>$texto</div>";
            if($tipo=="danger")
            echo "<div class='alert alert-danger'>$texto</div>";
        }
        
        
public function openDismissablealert($tipo,$texto)
        {
        
        if($tipo=="success")
            echo "<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <strong>success!</strong> $texto
            "; 
            if($tipo=="info")
            echo "<div class='alert alert-info alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <strong>info!</strong> $texto
            ";
            if($tipo=="warning")
            echo "<div class='alert alert-warning alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <strong>Warning!</strong> $texto
            ";
            if($tipo=="danger")
            echo "<div class='alert alert-danger alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <strong>danger!</strong> $texto
            ";    
        }
        
public function closeDismissablealert()
        {
        echo "</div>";
        }
        
public function openprogressbar($value)
{
echo    "<div class='progress progress-striped active'>
  <div class='progress-bar'  role='progressbar' aria-valuenow='$value' aria-valuemin='0' aria-valuemax='100' style='width: $value%'>
    <span class='sr-only'>$value% Complete</span>
  </div>"
;
}

public function closeprogressbar()
{
    echo "</div>";
}


public function panel($tipe,$contend)
    {
    if($tipe=="primary")
    echo "<div class='panel panel-primary'>$contend</div>";
    if($tipe=="success")
    echo "<div class='panel panel-success'>$contend</div>";
    if($tipe=="info")
    echo "<div class='panel panel-info'>$contend</div>";
    if($tipe=="warning")
    echo "<div class='panel panel-warning'>$contend</div>";
    if($tipe=="danger")
    echo "<div class='panel panel-danger'>$contend</div>";

    }
    
    public function modal($id)
    {
    echo "<button class='btn btn-primary btn-lg' data-toggle='modal' data-target='#$id'>
  Launch demo modal
</button>

<div class='modal fade' id='$id' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
  <div class='modal-dialog'>
    
    <div class='modal-content'>
      <div class='modal-header'>
        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
        <h4 class='modal-title' id='myModalLabel'>Modal title</h4>
      </div>
      <div class='modal-body'>
        ...
      </div>
      <div class='modal-footer'>
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
        <button type='button' class='btn btn-primary'>Save changes</button>
      </div>
    </div>
  </div>
</div>";
    
                
    }
    
    public function tooltip($nombre,$contenido)
    {
    echo "<script type='text/javascript'>
$(document).ready(function(){
    $('.tooltip-exam a').tooltip({
        placement : 'top'
    });
});
</script>";
    echo "<div class='bs-example'> 
    <ul class='tooltip-exam list-inline'>
        <li><a href='#' data-toggle='tooltip' data-original-title='$nombre'>$contenido</a></li>
            
    </ul>
</div>";
    
    }
    
    public function accordion()
    {
   $titulos=array("titulo1","titulo2","titulo3");
   $informacion=array("info1","info2","info3");
   
   $cantidad=count($titulos);
   
    $html="<div class='panel-group' id='accordion'>";
  
 for ($i=0;$i<$cantidad;$i++)
 {
  $html.="
  <div class='panel panel-default'>
    <div class='panel-heading'>
      <h4 class='panel-title'>
        <a data-toggle='collapse' data-parent='#accordion' href='#collapse".$i."'>
          Collapsible Group Item #1
        </a>
      </h4>
    </div>
    <div id='collapse".$i."' class='panel-collapse collapse in'>
      <div class='panel-body'>
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>";
 }
$html.="</div>";

echo $html;
  }
    
public function ajax($nombrediv,$nombrefuncion,$texto,$archivo)
  {
  echo "<script>
function $nombrefuncion()
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('$nombrediv').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open('GET','$archivo',true);
xmlhttp.send();
}
</script>
<div id='$nombrediv'><h2></h2>$texto</div>
<button type='button' onclick='$nombrefuncion()'>Change Content</button>";  
}

}

?>
