<?php

/**
 * This is the model class for table "alma_ciudades".
 *
 * The followings are the available columns in table 'alma_ciudades':
 * @property integer $ciu_id
 * @property string $ciu_nombre
 * @property string $ciu_codigodane
 * @property integer $mun_id
 * @property integer $usucreacion
 * @property string $feccreacion
 * @property integer $usuactualizacion
 * @property string $fecactualizacion
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property AlmaVehiculos[] $almaVehiculoses
 * @property AlmaVehiculos[] $almaVehiculoses1
 * @property AlmaEstcombustible[] $almaEstcombustibles
 * @property AlmaRutas[] $almaRutases
 * @property AlmaRutas[] $almaRutases1
 * @property AlmaBodegas[] $almaBodegases
 * @property AlmaClientes[] $almaClientes
 * @property AlmaClientes[] $almaClientes1
 * @property AlmaAlmacenes[] $almaAlmacenes
 * @property AlmaChecklist[] $almaChecklists
 * @property AlmaDestinatarios[] $almaDestinatarioses
 * @property AlmaMunicipios $mun
 * @property AlmaContenedores[] $almaContenedores
 * @property AlmaAseguradoras[] $almaAseguradorases
 * @property AlmnUsuarios[] $almnUsuarioses
 * @property AlmaTerceros[] $almaTerceroses
 * @property AlmaTerceros[] $almaTerceroses1
 * @property AlmaTerceros[] $almaTerceroses2
 * @property AlmaTerceros[] $almaTerceroses3
 * @property AlmaTerceros[] $almaTerceroses4
 * @property AlmaBancos[] $almaBancoses
 * @property AlmaEmpresas[] $almaEmpresases
 * @property AlmaRemitentes[] $almaRemitentes
 * @property AlmaPuecontrol[] $almaPuecontrols
 * @property AlmaZonas[] $almaZonases
 * @property AlmaFlociudad[] $almaFlociudads
 * @property AlmaCencosto[] $almaCencostos
 */
class AlmaCiudades extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alma_ciudades';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ciu_nombre', 'required'),
			array('mun_id, usucreacion, usuactualizacion, habilitado', 'numerical', 'integerOnly'=>true),
			array('ciu_nombre', 'length', 'max'=>100),
			array('ciu_codigodane, feccreacion, fecactualizacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ciu_id, ciu_nombre, ciu_codigodane, mun_id, usucreacion, feccreacion, usuactualizacion, fecactualizacion, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'almaVehiculoses' => array(self::HAS_MANY, 'AlmaVehiculos', 'ciu_autoriza_id'),
			'almaVehiculoses1' => array(self::HAS_MANY, 'AlmaVehiculos', 'ciu_referencia_id'),
			'almaEstcombustibles' => array(self::HAS_MANY, 'AlmaEstcombustible', 'ciu_id'),
			'almaRutases' => array(self::HAS_MANY, 'AlmaRutas', 'ciuori_id'),
			'almaRutases1' => array(self::HAS_MANY, 'AlmaRutas', 'ciudest_id'),
			'almaBodegases' => array(self::HAS_MANY, 'AlmaBodegas', 'ciu_id'),
			'almaClientes' => array(self::HAS_MANY, 'AlmaClientes', 'ciu_id'),
			'almaClientes1' => array(self::HAS_MANY, 'AlmaClientes', 'cli_ciu_contacto_id'),
			'almaAlmacenes' => array(self::HAS_MANY, 'AlmaAlmacenes', 'ciu_id'),
			'almaChecklists' => array(self::HAS_MANY, 'AlmaChecklist', 'ciu_id'),
			'almaDestinatarioses' => array(self::HAS_MANY, 'AlmaDestinatarios', 'ciu_id'),
			'mun' => array(self::BELONGS_TO, 'AlmaMunicipios', 'mun_id'),
			'almaContenedores' => array(self::HAS_MANY, 'AlmaContenedores', 'ciu_id'),
			'almaAseguradorases' => array(self::HAS_MANY, 'AlmaAseguradoras', 'ciu_id'),
			'almnUsuarioses' => array(self::HAS_MANY, 'AlmnUsuarios', 'ciu_id'),
			'almaTerceroses' => array(self::HAS_MANY, 'AlmaTerceros', 'ciu_conemergencia_id'),
			'almaTerceroses1' => array(self::HAS_MANY, 'AlmaTerceros', 'ciu_expedicion_id'),
			'almaTerceroses2' => array(self::HAS_MANY, 'AlmaTerceros', 'ciu_radicacion_id'),
			'almaTerceroses3' => array(self::HAS_MANY, 'AlmaTerceros', 'ciu_referencia_id'),
			'almaTerceroses4' => array(self::HAS_MANY, 'AlmaTerceros', 'ciu_residencia_id'),
			'almaBancoses' => array(self::HAS_MANY, 'AlmaBancos', 'ciu_id'),
			'almaEmpresases' => array(self::HAS_MANY, 'AlmaEmpresas', 'ciu_id'),
			'almaRemitentes' => array(self::HAS_MANY, 'AlmaRemitentes', 'ciu_id'),
			'almaPuecontrols' => array(self::HAS_MANY, 'AlmaPuecontrol', 'ciu_id'),
			'almaZonases' => array(self::HAS_MANY, 'AlmaZonas', 'ciu_id'),
			'almaFlociudads' => array(self::HAS_MANY, 'AlmaFlociudad', 'ciu_origen_id'),
			'almaCencostos' => array(self::HAS_MANY, 'AlmaCencosto', 'ciu_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ciu_id' => 'Ciu',
			'ciu_nombre' => 'Ciu Nombre',
			'ciu_codigodane' => 'Ciu Codigodane',
			'mun_id' => 'Mun',
			'usucreacion' => 'Usucreacion',
			'feccreacion' => 'Feccreacion',
			'usuactualizacion' => 'Usuactualizacion',
			'fecactualizacion' => 'Fecactualizacion',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ciu_id',$this->ciu_id);
		$criteria->compare('ciu_nombre',$this->ciu_nombre,true);
		$criteria->compare('ciu_codigodane',$this->ciu_codigodane,true);
		$criteria->compare('mun_id',$this->mun_id);
		$criteria->compare('usucreacion',$this->usucreacion);
		$criteria->compare('feccreacion',$this->feccreacion,true);
		$criteria->compare('usuactualizacion',$this->usuactualizacion);
		$criteria->compare('fecactualizacion',$this->fecactualizacion,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlmaCiudades the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
